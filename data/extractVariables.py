# With this script you can extract the variables in the xml weight files
# It will then print the lines to be pasted in the BDTClassifiers

import sys
catName=sys.argv[1]
weightfilename=sys.argv[2]

templateLine="readerMap[\"CATNAME\"]->AddVariable(VARIABLENAME, &variableMap[VARIABLENAME]);"

inf=open(weightfilename,"r")
lines=list(inf)

for l in lines:
  if "Expression" in l:
    varname="EMPTYVAR"
    spacesplitline=l.split(" ")
    for subline in spacesplitline:
      if "Expression" in subline:
        varname=subline.split("=")[1]
    print templateLine.replace("CATNAME",catName).replace("VARIABLENAME",varname)
